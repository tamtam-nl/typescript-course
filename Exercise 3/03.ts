// These are the only colors allowed for an application
const colors = {
    BRAND_ONE: '#FFFFFF',
    BRAND_TWO: '#123456',
    UNICORN_PINK: 'pink'
}

// Task: create an anonymous function getNewColor() that only 
//       allows the above three colors to be accepted as String

getNewColor('BRAND_ONE') // Yes!
getNewColor('UNICORN_RED') // Nope!