// Task: create a myUnionNumber union with number or numbers array as allowed types
var myUnionNumber;

// Task: set myUnionNumber so that it passes the first if-statement
myUnionNumber;

if (typeof myUnionNumber === 'number') {
    console.log('myUnionNumber is a number');
}
else {
    console.log('myUnionNumber is now an object');
}

// Task: set myUnionNumber so that it passes the first if-statement
myUnionNumber;

if (typeof myUnionNumber === 'object') {
    console.log('myUnionNumber is now an object');
}
else {
    console.log('myUnionNumber is a number');
}

// Task: use a switch statement around the typeof command for myUnionNumber 
//       so that the second case is true
var myVarType;

switch (myVarType) {
    case 'number':
        {
            console.log('myUnionNumber is a number');

            break;
        }
    case 'object':
        {
            console.log('myUnionNumber is now an object');
            break;
        }
}