
// Task: rename this file to be .ts instead of .js
// Task: set the parameters to ES6
// Task: See the errors come up
var depsters = ['Nick', 'Arthur', 'Jeroen', 'Daphne', 'Kees'];
var employeesAmount = 750;
var attendingTSCourse = true;

depsters.push(12); // Added to the array in JS
employeesAmount.join(); // Error in JS
attendingTSCourse + employeesAmount; // gives 751 in JS