
// Task: create an interface for Pet with name, age, weight
interface Pet {
}

// Task: create an array with type Pet
var petsArray = [];

// Task: add Pet Jasmin, born 9 years ago and weighs a mere 55 Kg


// Task: create a const Pet Roxie, born 6 years ago and weighs a nice 85 Kg
var roxie;

// Task: add Roxie to the petsArray


// Task: Create a const myPets of type Pet with 
//       Teddy, age 1 and 4 Kg
//       Rush, age 15 and weight 45
const myPets;

// Task: Add myPets to the petsArray 
