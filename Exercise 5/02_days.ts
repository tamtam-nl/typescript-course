
// Task: create an enum Weekday with all days of the week
enum Weekday {}

// Task: write a function isBusinessDay to check if a day is a businessday
//       so that the first two console.logs do not give an error
function isBusinessDay() {}

// add the isBusinessDay function to Weekday as a method, e.g. Weekday.isBusinessDay()

console.log(isBusinessDay(mon)); // true
console.log(isBusinessDay(sun)); // false
console.log(Weekday.isBusinessDay(sun)); // false